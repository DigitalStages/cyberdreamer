//Eye protection module for Cyberdreamer
//created by Jens Meisner
//License cc-by-sa 4.0

$fn=80;

difference()
{
    union()
    {
        cylinder(d=50, h=2);
        cylinder(d=42, h=7);
        translate([0, 0, 7])
        scale([1, 2/3, 1])
        cylinder(d=70, h=10);
    }
    union()
    {
        cylinder(d=38, h=20);
        translate([0, 0, 3.2])
        cube([55, 15, 6.4], center=true);
        
        translate([-5, 0, 19.5])
        rotate([0, 5, 0])
        scale([1, 4/5, 1/4])
        sphere(44);
    }
}
