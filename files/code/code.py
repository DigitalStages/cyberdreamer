# CPX M0 Dream Monocle - Ben Jabituya
import time
import board
import neopixel
import digitalio
from analogio import AnalogIn
import math
import microcontroller
from adafruit_circuitplayground.express import cpx

RED = (255, 0, 0)
GREEN = (0, 255, 0)
OFF = (0, 0, 0)

# ADVANCED SETTINGS
cooldown_delay = 200
passive_mode = False
led_brightness = 0.06
time_delay = 2
red_cycle_count = 8
log_limit = 2000
function_start_time = 0

analog1in = AnalogIn(board.IR_PROXIMITY)

ir_led = digitalio.DigitalInOut(board.IR_TX)
ir_led.direction = digitalio.Direction.OUTPUT

def set_delay():
    global time_delay
    global boot_time
    for led in range(time_delay):
        cpx.pixels[led] = RED
        cpx.pixels.show()
    boot_time = time.monotonic()
    time.sleep(1)
    start_time = time.monotonic()
    timegap = time.monotonic() - start_time

    while timegap < 5:
        if cpx.button_b:
            time.sleep(0.2)
            start_time = time.monotonic()
            if time_delay == 10:
                time_delay = 1
            else:
                time_delay += 1
            cpx.pixels.fill(OFF)
            cpx.pixels.show()

            for led in range(time_delay):
                cpx.pixels[led] = RED
                cpx.pixels.show()
        timegap = time.monotonic() - start_time
    save_settings()
    red_cycle(0.4)
    cpx.pixels.fill(OFF)
    cpx.pixels.show()

def set_brightness():
    global led_brightness
    cpx.pixels.fill(GREEN)
    cpx.pixels.show()
    time.sleep(1)
    start_time = time.monotonic()
    timegap = time.monotonic() - start_time
    while timegap < 5:
        if cpx.button_a:
            time.sleep(0.2)
            start_time = time.monotonic()
            if led_brightness > 0.1:
                led_brightness = 0.02
            else:
                led_brightness += 0.02
            cpx.pixels.brightness = led_brightness
            cpx.pixels.fill(GREEN)
            cpx.pixels.show()
        timegap = time.monotonic() - start_time
    save_settings()
    red_cycle(0.4)
    cpx.pixels.fill(OFF)
    cpx.pixels.show()

def load_settings(fname):
    global led_brightness
    global time_delay
    with open(fname) as f:
        first_line = f.readline()
        settings = first_line.split("|")
        led_brightness = float(settings[0])
        time_delay = int(settings[1])

def save_settings():
    global led_brightness
    global time_delay

    settings_string = str(led_brightness) + "|" + str(time_delay)
    try:
        with open('/lucid_settings.txt', "w") as f:
            f.write(settings_string)
            f.flush
    except OSError as e:
        print(e)
        pass

def red_cycle(wait):
    for i in range(10):
        cpx.pixels[i] = (255, 0, 0)
        if i < 5:
            cpx.pixels[i+5] = (255, 0, 0)
        elif i > 5:
            cpx.pixels[i-5] = (255, 0, 0)
        cpx.pixels.show()
        time.sleep(wait)
        cpx.pixels[i] = OFF
        if i < 5:
            cpx.pixels[i+5] = OFF
        elif i > 5:
            cpx.pixels[i-5] = OFF
        cpx.pixels.show()
	time.sleep(wait)

def get_stanard_deviation(type):
    counter = 30
    value_samples = []
    function_start_time = time.monotonic()

    if type == "IR":
        for x in range(counter):
            ir_led.value = True
            time.sleep(0.005)
            ir_led.value = False
            time.sleep(0.001)
            value = analog1in.value
            if value > 0:
                value_samples.append(value)
            time.sleep(0.19)

    else:
        for x in range(counter):
            x, y, z = cpx.acceleration
            all_angles = abs(x), abs(y), abs(z)
            value_samples.append(sum(all_angles))
            time.sleep(0.05)

    if len(value_samples) > 10:
        write_to_file(str(value_samples))
        average_value = sum(value_samples)/len(value_samples)
        variance = sum(pow(x-average_value, 2) for x in value_samples) / len(value_samples)
        standard_deviation = math.sqrt(variance)
        return standard_deviation

def check_for_movement():
    global log_counter
    global log_eye_movement
    global face_movement
    function_start_time = time.monotonic()
    timer = 0

    face_movement.clear()
    while timer < 20:
        log_face_movement = get_stanard_deviation("ACCEL")
        face_movement.append(log_face_movement)
        time.sleep(5)
        timer = time.monotonic() - function_start_time
    print("face movement count = " + str(len(face_movement)))
    log_eye_movement = get_stanard_deviation("IR")

def write_to_file(sentence):
    global logfile_linecount
    global log_limit

    if logfile_linecount < log_limit:
        try:
            with open("/logfile.csv", "a") as fp:
                fp.write(sentence + ' \n')
                fp.flush()
                logfile_linecount += 1
        except OSError as e:
            print(e)
            pass

def Average(lst):
    return sum(lst) / len(lst)

# MAIN LOOP
boot_time = time.monotonic()
face_movement = []
log_counter = 0
log_eye_movement = 0
counter = 0
logfile_linecount = 0
REM_count = 0

try:
    file = open("/logfile.csv", "w")
    file.close()
except OSError as e:
    print(e)
    pass

load_settings("/lucid_settings.txt")

while True:

    if cpx.button_a:
        set_brightness()

    if cpx.button_b:
        set_delay()

    timegap = time.monotonic() - boot_time

    if timegap > 20:
        real_timedelay = time_delay * 60 * 30
        if time_delay == 1:
            real_timedelay = 0

        if timegap > real_timedelay:
            print("Check for movement")
            check_for_movement()
            counter += 1
            average_face_movement = Average(face_movement) * 100
            timer = time.monotonic() - boot_time
            logstring = str(round(timer)) + "," + str(round(log_eye_movement)) + "," + str(round(average_face_movement)) + "," + str(round(microcontroller.cpu.temperature))
            print(logstring)
            write_to_file(logstring)

            if log_eye_movement > 600 and average_face_movement < 50:
                print("REM detected, wait for second occurance before cueing")
                if REM_count > 0:
                    print("trigger Red cycle")
                    write_to_file("sweet dreams")
                    if passive_mode == False:
                        for x in range(red_cycle_count):
                            red_cycle(0.4)
                else:
                    REM_count += 1

            if counter == 5:
                REM_count = 0
                counter = 0
                time.sleep(cooldown_delay)